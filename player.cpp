#include "player.h"
#include <vector>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
     
     board = Board();
     ai_side = side;
     
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
    Side other_side;
    Move *m;
    std::vector<Move*> possible;
    int score = -100;
    Move *best;
    
    if (ai_side == BLACK)
    {
		other_side = WHITE;
	}
	
	else
	{
		other_side = BLACK;
	}
	
    board.doMove(opponentsMove, other_side);
    Board *board_copy = board.copy();
    
    if (!board_copy->hasMoves(ai_side))
    {
		return NULL;
	}
	
    for (int i = 0; i < 8; i++)
    {
		for (int j = 0; j < 8; j++)
		{
			m = new Move(i, j);
			if (board_copy->checkMove(m, ai_side))
			{
				possible.push_back(m);	
			}
		}
	}
	
	for (int i = 0; i < (int) possible.size(); i++)
	{
		int temp;
		board_copy = board.copy();
		board_copy->doMove(possible[i], ai_side);
		if (ai_side == BLACK)
		{
			temp = board_copy->countBlack() - board_copy->countWhite();
		}
		else
		{
			temp = board_copy->countWhite() - board_copy->countBlack();
		}
		if ((possible[i]->x == 0 || possible[i]->x == 7) && (possible[i]->y == 0 || possible[i]->y == 7))
		{
			temp += 7;
		}
		else if ((possible[i]->x == 1 || possible[i]->x == 6) && (possible[i]->y == 1 || possible[i]->y == 6))
		{
			temp -= 7
		else if ((possible[i]->x == 1 || possible[i]->x == 6) && (possible[i]->y == 1 || possible[i]->y == 6))
		{
			temp -= 5;
		}
		else if ((possible[i]->x == 0 || possible[i]->x == 7) && (possible[i]->y == 1 || possible[i]->y == 6))
		{
			temp -= 5;
		}
		else if ((possible[i]->x == 1 || possible[i]->x == 6) && (possible[i]->y == 0 || possible[i]->y == 7))
		{
			temp -= 5;
		}
		else if (possible[i]->x == 0 || possible[i]->x == 7)
		{
			temp += 3;
		}
		else if (possible[i]->y == 0 || possible[i]->y == 7)
		{
			temp += 3;
		}
		
		if (temp > score)
		{
			score = temp;
			best = possible[i];
		}
	}
	board.doMove(best, ai_side);
	return best;
}
