Last week, I didn't take into account that placing a piece in the spot diagonally adjacent
to corners allows the opponent to place his/her pieces in the corner, which is a huge
disadvantage for me. I added another else if statement this week that subtracts 7 from
the heuristic score of positions diagonally adjacent to corners. This is the same number
added to the heuristic score of corner positions. Improving my heuristic allows me to
more consistently beat SimplePlayer and ConstantTimePlayer. I think this strategy
works because it takes into consideration another scenario that could not work in my
favor and ensures it occurs as little as possible.